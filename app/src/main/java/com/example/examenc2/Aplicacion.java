package com.example.examenc2;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.VentasDb;

public class Aplicacion extends Application {

    public static ArrayList<Ventas> ventas;
    private VentasAdapter ventasAdapter;
    private VentasDb ventasDb;

    public ArrayList<Ventas> getVentas() {
        return ventas;
    }

    public VentasAdapter getAdaptador() {
        return ventasAdapter;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ventasDb = new VentasDb(getApplicationContext());
        ventas = ventasDb.allVentas();
        ventasDb.openDataBase();

        Log.d("", "onCreate: Tamaño array list: " + this.ventas.size());
        this.ventasAdapter = new VentasAdapter(this.ventas, this);
    }

    public void agregarVenta(Ventas venta) {
        ventasDb.openDataBase();
        long resultado = ventasDb.insertVenta(venta);
        if (resultado != -1) {
            venta.setId((int) resultado);
            ventas.add(venta);
            ventasAdapter.notifyDataSetChanged();
        }
    }
}
