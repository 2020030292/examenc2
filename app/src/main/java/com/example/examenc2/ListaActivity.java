package com.example.examenc2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import Modelo.VentasDb;

public class ListaActivity extends AppCompatActivity {
    private ListView listViewVentas;
    private int posicion;
    private TextView textViewTotalVentas;
    private Aplicacion app;
    private Button buttonRegresar;

    private ArrayList<Ventas> registroVentas;
    private double totalVentas;

    private Ventas ventas;

    private VentasDb ventasDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        this.ventas= new Ventas();
        this.posicion = -1;
        this.app = (Aplicacion) getApplication();
        this.ventasDb = new VentasDb(getApplicationContext());
        // Inicializar vistas
        listViewVentas = findViewById(R.id.listViewVentas);
        textViewTotalVentas = findViewById(R.id.textViewTotalVentas);
        this.listViewVentas.setAdapter(app.getAdaptador());
        this.buttonRegresar = findViewById(R.id.buttonRegresar);
        app.getVentas().clear();
        app.getVentas().addAll(ventasDb.allVentas());
        ((VentasAdapter)listViewVentas.getAdapter()).notifyDataSetChanged();
        this.buttonRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) { buttonRegresar(); }
        });

        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { listViewVentas(view); }
        });

        // Obtener la lista de ventas desde la base de datos y asignarla al adaptador
        registroVentas = ventasDb.allVentas();
        app.getAdaptador().setVentasList(registroVentas);
        listViewVentas.setAdapter(app.getAdaptador());

        // Calcular el total de ventas
        totalVentas = calcularTotalVentas(registroVentas);
        textViewTotalVentas.setText("Total de ventas: $" + String.format("%.2f", totalVentas));

    }

    private double calcularTotalVentas(ArrayList<Ventas> ventasList) {
        double total = 0;
        for (Ventas venta : ventasList) {
            total += venta.getCantidad() * venta.getPrecio();
        }
        return total;
    }

    private void buttonRegresar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres regresar?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Regresar al MainActivity
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void listViewVentas(View view) {
        posicion = listViewVentas.getPositionForView(view);
        ventas = app.getVentas().get(posicion);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK) {
            app.getVentas().clear();
            app.getVentas().addAll(ventasDb.allVentas());
            app.getAdaptador().setVentasList(app.getVentas());
            app.getAdaptador().setVentasList(app.getVentas());
            app.getAdaptador().notifyDataSetChanged();
        }

        this.posicion = -1;
        this.ventas = null;
    }
}



