package com.example.examenc2;

import java.io.Serializable;

public class Ventas implements Serializable {
    private int numBomba;
    private String tipoGasolina;
    private double precio;
    private double cantidad;

    public Ventas(int numBomba, String tipoGasolina, double precio, double cantidad) {
        this.numBomba = numBomba;
        this.tipoGasolina = tipoGasolina;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public Ventas() {

    }

    // Métodos getter y setter

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public String getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public void setId(int anInt) {
    }
}

