package com.example.examenc2;

public class BombaGasolina {
    private int numBomba;
    private String tipoGasolina;
    private double contadorVentas;
    private double capacidad;

    public BombaGasolina(int numBomba, String tipoGasolina, double capacidad) {
        this.numBomba = numBomba;
        this.tipoGasolina = tipoGasolina;
        this.contadorVentas = 0;
        this.capacidad = capacidad;
    }

    // Métodos getter y setter

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public String getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public double getContadorVentas() {
        return contadorVentas;
    }

    public void setContadorVentas(double contadorVentas) {
        this.contadorVentas = contadorVentas;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }
}

