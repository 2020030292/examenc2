package com.example.examenc2;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class VentasAdapter extends ArrayAdapter<Ventas> {
    private Application context;
    private ArrayList<Ventas> ventas;
    private ArrayList<Ventas> listaVentas;
    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public VentasAdapter(ArrayList<Ventas> listaVentas, Application context) {
        super(context, 0, listaVentas);
        this.listaVentas = listaVentas;
        this.ventas = new ArrayList<>(listaVentas);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        // Obtener la venta en la posición actual
        Ventas venta = getItem(position);

        // Verificar si ya existe una vista que se pueda reutilizar
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        // Configurar el contenido de la vista con la información de la venta
        TextView textViewVenta = convertView.findViewById(android.R.id.text1);
        if (venta != null) {
            double costoTotal = venta.getCantidad() * venta.getPrecio();

            // Formatear el costo total con dos decimales
            DecimalFormat df = new DecimalFormat("#.##");
            String ventaInfo = "" + venta.getCantidad() +
                    " x " + venta.getPrecio() +
                    " = " + df.format(costoTotal);
            textViewVenta.setText(ventaInfo);
        }

        return convertView;
    }

    public void setVentasList(ArrayList<Ventas> listaVentas) {
        this.ventas.clear();
        this.ventas.addAll(listaVentas);
        notifyDataSetChanged();
    }
}

