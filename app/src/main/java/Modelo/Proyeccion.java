package Modelo;

import android.database.Cursor;

import com.example.examenc2.Ventas;

import java.util.ArrayList;

public interface Proyeccion {
    Ventas getVenta(int id);
    ArrayList<Ventas> allVentas();
    Ventas readVenta(Cursor cursor);
}

