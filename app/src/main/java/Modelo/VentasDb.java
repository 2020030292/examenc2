package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.examenc2.Ventas;

import java.util.ArrayList;

public class VentasDb implements Persistencia, Proyeccion {

    private Context context;
    private VentasDbHelper helper;
    private SQLiteDatabase db;

    public VentasDb(Context context, VentasDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public VentasDb(Context context) {
        this.context = context;
        this.helper = new VentasDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertVenta(Ventas venta) {
        ContentValues values = new ContentValues();
        values.put(DefineTable.Ventas.COLUMN_NAME_NUM_BOMBA, venta.getNumBomba());
        values.put(DefineTable.Ventas.COLUMN_NAME_TIPO_GASOLINA, venta.getTipoGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_PRECIO, venta.getPrecio());
        values.put(DefineTable.Ventas.COLUMN_NAME_CANTIDAD, venta.getCantidad());

        this.openDataBase();
        long id = db.insert(DefineTable.Ventas.TABLE_NAME, null, values);
        this.closeDataBase();

        return id;
    }

    @Override
    public Ventas getVenta(int id) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                DefineTable.Ventas.COLUMN_NAME_ID + " = ?",
                new String[]{String.valueOf(id)},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            Ventas venta = readVenta(cursor);
            cursor.close();
            return venta;
        }
        return null;
    }

    @Override
    public ArrayList<Ventas> allVentas() {
        this.openDataBase();
        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                null, null, null, null, null);

        ArrayList<Ventas> ventasList = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Ventas venta = readVenta(cursor);
            ventasList.add(venta);
            cursor.moveToNext();
        }

        cursor.close();
        this.closeDataBase();

        return ventasList;
    }

    @Override
    public Ventas readVenta(Cursor cursor) {
        Ventas venta = new Ventas();
        venta.setId(cursor.getInt(0));
        venta.setNumBomba(cursor.getInt(1));
        venta.setTipoGasolina(cursor.getString(2));
        venta.setPrecio(cursor.getDouble(3));
        venta.setCantidad(cursor.getDouble(4));

        return venta;
    }
}